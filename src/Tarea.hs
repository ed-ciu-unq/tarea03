module Tarea where

-- Devuelve los números pares de una lista de números
soloPares :: [Int] -> [Int]
soloPares   []   = []
soloPares (x:xs) = if even x
                      then x: soloPares xs
                      else    soloPares xs


-- Dado un numero y una lista de numeros, devuelve una lista con los numeros
-- menores al dado
menoresQue :: Ord a => a -> [a] -> [a]
menoresQue = undefined


-- Dado un numero y una lista de numeros, devuelve una lista con los numeros
-- mayores o iguales al dado
mayoresOIgualesQue :: Ord a => a -> [a] -> [a]
mayoresOIgualesQue = undefined


-- Dado un elemento y una lista, saca los elementos iguales al dado
sacarIgualesA :: Eq a => a -> [a] -> [a]
sacarIgualesA = undefined


-- Dada una lista de pares de enteros y un entero, devuelve los pares cuyas dos
-- componentes sean mayores al numero dado
-- Ejemplo: paresMayoresA [(2,7),(4,5),(3,1)] 3 => [(4,5)]
paresMayoresA :: [(Int,Int)] -> Int ->  [(Int,Int)]
paresMayoresA = undefined


--Dada una lista de pares, devuelve aquellos cuyas componentes sean iguales
-- Ejemplo: soloParesIguales [(1,3),(3,3),(5,2)] => [(3,3)]
soloParesIguales :: Eq a => [(a,a)] -> [(a,a)]
soloParesIguales = undefined


-- Dada una lista que contiene listas, devolver la lista que contiene listas
-- no vacias
filtrarListasVacias :: [[a]] -> [[a]]
filtrarListasVacias = undefined



-- Dada una lista de Strings, devolver los strings que empiezan con la letra 'A'
empiezanConA :: [String] -> [String]
empiezanConA = undefined


-- Dada una lista de números, determinar si tiene la misma cantidad de números pares
-- que de números impares
igualCantDeParesQueDeImpares :: [Int] -> Bool
igualCantDeParesQueDeImpares = undefined
