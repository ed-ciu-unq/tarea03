# FRANKENSTEIN: Estructuras + Haskell + git

El proyecto Frankenstein consiste en una cantidad de tareas, en orden creciente de dificultad, que cubren el contenido de la parte de Haskell de Estructuras de Datos.

El objetivo principal en Frankenstein consiste en “matar los _undefined_”. Un _undefined_ representa un espacio en blanco que se debe completar con código que cumpla la funcionalidad esperada.

El comando `stack test` permite correr los tests de la tarea. Al principio todos ellos deben estar en rojo. La meta es que todos pasen a estar en verde.

## Tarea 03: Filtros sobre listas

En las funciones de manejo de listas que usan el patrón FILTER, la cantidad de los elementos de la lista resultante puede variar, pero los elementos son los mismos que en la lista de entrada.

Un filtro simplemente selecciona cuales items van a ser parte del resultado y cuales no. Los que van a ser filtrados, o sea, van a ser "quitados de la lista" son los que no son seleccionados para quedarse en la lista resultante.

Se recomienda usar las funciones de Haskell que en las tareas anteriores se introdujeron (las `AKA`)

Y como siempre en programación, muchas soluciones quedan mejor expresadas si utilizar funciones más pequeñas.

---

Autor: Román García (nykros@gmail.com)
