module TareaSpec (spec) where
import Test.Hspec
import Tarea

spec :: Spec
spec =
  describe "Pruebas para la tarea" $ do
    it "soloPares" $
      soloPares [3,5,2,7,-8] `shouldBe` [2,-8]

    it "menoresQue" $
      menoresQue 5 [3,5,2,7,-8] `shouldBe` [3,2,-8]

    it "mayoresOIgualesQue" $
      mayoresOIgualesQue 5 [3,5,2,7,-8] `shouldBe` [5,7]

    it "sacarIgualesA" $
      sacarIgualesA 'I' "MISSISSIPPI" `shouldBe` "MSSSSPP"

    it "soloParesIguales" $
      soloParesIguales [(1,3),(2,2),(5,2)] `shouldBe` [(2,2)]

    it "paresMayoresA" $
      paresMayoresA [(2,7),(4,5),(3,1)] 3 `shouldBe` [(4,5)]

    it "filtrarListasVacias" $
      filtrarListasVacias [[1],[],[4,2],[]] `shouldBe` [[1],[4,2]]

    it "empiezanConA" $
      empiezanConA ["Avanza","a","la","Alacena"] `shouldBe` ["Avanza","Alacena"]

    it "igualCantDeParesQueDeImpares False" $
      igualCantDeParesQueDeImpares [1,4,7,4,9] `shouldBe` False
      
    it "igualCantDeParesQueDeImpares True" $
      igualCantDeParesQueDeImpares [1,4,7,4] `shouldBe` True
